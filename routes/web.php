<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

Route::get('/', 'UsersController@index');
Route::get('/home', 'UsersController@index');
Route::get('/shop', 'UsersController@fnShop');
Route::get('/sales', 'UsersController@fnSales');
Route::get('/about', 'UsersController@fnAbout');
Route::get('/contact', 'UsersController@fnContact');
Route::get('/product-detail', 'UsersController@fnProductDetail');
Route::get('/contact', 'UsersController@fnContact');
Route::get('/cart', 'UsersController@fnCart');

//=================================================================================

/*Route::get('/admin', 'UsersController@fnGetAdminlogin');
Route::post('/adminLogin', 'SessionsController@fnDoAdminLogin');
Route::post('/adminLogout', 'SessionsController@destroy');*/

/*Route::get('/home', 'HomeController@index');*/

  Route::prefix('admin')->group(function() {
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
    Route::get('/create_prod_cat','CategoriesController@fnShowForm')->name('admin.create_category');
    Route::post('/create_prod_cat','CategoriesController@fnCreateCategory')->name('admin.create_category');
    Route::get('/', 'AdminController@index')->name('admin.dashboard');

  });
