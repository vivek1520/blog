<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
class UsersController extends Controller
{
    public function index()
    {
    	return view('index');
    }

    public function fnShop()
    {
    	return view('shop');
    }

    public function fnSales()
    {
    	return view('sales');
    }

    public function fnAbout()
    {
    	return view('about');
    }

    public function fnContact()
    {
    	return view('contact');
    }

    public function fnProductDetail()
    {
    	return view('sales.product-detail');
    }

    public function fnCart()
    {
    	return view('cart');
    }

    public function fnGetAdminlogin()
    {
    	return view('admin.admin_login');
    }
}
